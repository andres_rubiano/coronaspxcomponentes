﻿namespace Simplexity.Components.Messaging.Events
{
    public class EntityChanged
    {
    }

	public static class OperationTypes
	{
		public const string Create = "PUT";
		public const string Update = "PATCH";
		public const string Delete = "DELETE";
	}
}
