﻿using System.IO;
using System.Text;
using Newtonsoft.Json;
using Simplexity.Components.Serialization.JSON.JsonConverters;

namespace Simplexity.Components.Serialization.JSON
{
    public class JSONSerializer : ISerializer
    {
	    private readonly JsonSerializer _jsonSerializer;
	    private readonly Encoding _encoding;

	    public JSONSerializer()
			: this(new DictionaryJsonConverter())
	    {

	    }

		public JSONSerializer(params JsonConverter[] converters)
			: this(new JsonSerializerSettings() { Converters = converters })
		{
		}

		public JSONSerializer(JsonSerializerSettings settings)
	    {
			_jsonSerializer = JsonSerializer.Create(settings);
			_encoding = Encoding.UTF8;
	    }

	    public int Serialize<T>(T obj, byte[] bytes, int offset)
	    {
		    string serialObj = SerializeToString(obj);
		    return _encoding.GetBytes(serialObj, 0, serialObj.Length, bytes, offset);
	    }

	    public byte[] Serialize<T>(T obj)
	    {
		    return _encoding.GetBytes(SerializeToString(obj));
	    }

	    public string SerializeToString<T>(T obj)
	    {
			using (var writer = new StringWriter())
			{
				_jsonSerializer.Serialize(writer, obj);
				return writer.ToString();
			}
	    }

	    public T Deserialize<T>(byte[] bytes)
	    {
		    return Deserialize<T>(bytes, 0, bytes.Length);
	    }

	    public T Deserialize<T>(byte[] bytes, int offset, int count)
	    {
			return DeserializeFromString<T>(_encoding.GetString(bytes, offset, count));
	    }

	    public T DeserializeFromString<T>(string serialObj)
	    {
			using (var reader = new JsonTextReader(new StringReader(serialObj)))
			{
				return _jsonSerializer.Deserialize<T>(reader);
			}
	    }
    }
}
