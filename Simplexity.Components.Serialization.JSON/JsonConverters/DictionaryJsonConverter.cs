﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Simplexity.Components.Serialization.JSON.JsonConverters
{
	internal class DictionaryJsonConverter : JsonConverter
	{
		public override bool CanWrite
		{
			get { return false; }
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotSupportedException("This converter should only be used while deserializing.");
		}

		public override object ReadJson(
			JsonReader reader,
			Type objectType,
			object existingValue,
			JsonSerializer serializer)
		{
			switch (reader.TokenType)
			{
				case JsonToken.Null:
					return null;
				case JsonToken.StartObject:
					return serializer.Deserialize<IDictionary<string, object>>(reader);
				case JsonToken.StartArray:
					return serializer.Deserialize<IEnumerable<object>>(reader);
				default:
					return serializer.Deserialize(reader);
			}
		}

		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(object);
		}
	}
}