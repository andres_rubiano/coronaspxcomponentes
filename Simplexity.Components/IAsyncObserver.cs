﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Simplexity.Components
{

    /// <summary>
    /// Interfaz observador (Patrón Observador) con asincronía.
    /// </summary>
    /// <typeparam name="T">Tipo del objeto que provee información de las notificaciones.</typeparam>
    public interface IAsyncObserver<in T> : IObserver<T>
    {
        /// <summary>
        /// Provee al observador nueva información.
        /// </summary>
        /// <param name="next">Objeto que contiene la información.</param>
        /// <returns>Tarea.</returns>
        Task OnNextAsync(T next);

        /// <summary>
        /// Provee al observador nueva información.
        /// </summary>
        /// <param name="next">Objeto que contiene la información.</param>
        /// <param name="cancellationToken">Token para cancelación del procesamiento que se este haciendo con el objeto de notificación en la tarea asíncrona retornada..</param>
        /// <returns>Tarea asíncrona.</returns>
        Task OnNextAsync(T next, CancellationToken cancellationToken);

        /// <summary>
        /// Informa al observador que el observable ha tenido una excepción.
        /// </summary>
        /// <param name="exception">Excepción que ocurrío en el observable.</param>
        /// <returns>Tarea asíncrona.</returns>
        Task OnErrorAsync(Exception exception);

        /// <summary>
        /// Informa al observador que el observable ha tenido una excepción.
        /// </summary>
        /// <param name="exception">Excepción que ocurrío en el observable.</param>
        /// <param name="cancellationToken">Token para cancelación del procesamiento que se este haciendo con la excepción recibida en la tarea asíncrona retornada.</param>
        /// <returns>Tarea asíncrona.</returns>
        Task OnErrorAsync(Exception exception, CancellationToken cancellationToken);

        /// <summary>
        /// Informa al observador que el observable ha terminado de enviar datos. Es decir, no recibirá más información.
        /// </summary>
        /// <returns>Tarea asíncrona.</returns>
        Task OnCompletedAsync();

        /// <summary>
        /// Informa al observador que el observable ha terminado de enviar datos. Es decir, no recibirá más información.
        /// </summary>
        /// <param name="cancellationToken">Token para cancelación del procesamiento que se este haciendo en la tarea asíncrona retornada.</param>
        /// <returns>Tarea asíncrona.</returns>
        Task OnCompletedAsync(CancellationToken cancellationToken);
    }
}
