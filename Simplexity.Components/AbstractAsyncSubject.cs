﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Simplexity.Components
{
    /// <summary>
    /// Implementación base de un 'subject' asíncrono (IAsyncSubject). Un 'subject' es un objeto que es a su vez observador y observable.
    /// </summary>
    /// <typeparam name="TIn">Tipo del objeto que provee información de las notificaciones recibidas.</typeparam>
    /// <typeparam name="TOut">Tipo del objeto que provee información de las notificaciones generadas.</typeparam>
    public abstract class AbstractAsyncSubject<TIn, TOut> : AbstractAsyncObservable<TOut>, IAsyncSubject<TIn, TOut>
    {
        public void OnNext(TIn value)
        {
            OnNextAsync(value, CancellationToken.None).Wait();
        }

        public void OnError(Exception error)
        {
            OnErrorAsync(error, CancellationToken.None).Wait();
        }

        public void OnCompleted()
        {
            OnCompletedAsync(CancellationToken.None).Wait();
        }

        public Task OnNextAsync(TIn next)
        {
            return OnNextAsync(next, CancellationToken.None);
        }

        public Task OnErrorAsync(Exception exception)
        {
            return OnErrorAsync(exception, CancellationToken.None);
        }

        public Task OnCompletedAsync()
        {
            return OnCompletedAsync(CancellationToken.None);
        }

        public abstract Task OnNextAsync(TIn next, CancellationToken cancellationToken);

        public abstract Task OnErrorAsync(Exception exception, CancellationToken cancellationToken);

        public abstract Task OnCompletedAsync(CancellationToken cancellationToken);
    }
}
