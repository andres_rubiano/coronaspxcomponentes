﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Simplexity.Components
{

    /// <summary>
    /// Adaptador de observador síncrono a observador asíncrono.
    /// </summary>
    /// <typeparam name="T">Tipo del objeto que provee información de las notificaciones.</typeparam>
    public sealed class ObserverToAsyncAdapter<T> : IAsyncObserver<T>
    {
        private readonly IObserver<T> _observer;
        private readonly TaskCreationOptions _taskCreationOptions;
        private readonly TaskScheduler _taskScheduler;

        public ObserverToAsyncAdapter(IObserver<T> observer)
            : this (observer, TaskCreationOptions.None, TaskScheduler.Current)
        { }

        public ObserverToAsyncAdapter(IObserver<T> observer, TaskCreationOptions taskCreationOptions)
            : this(observer, taskCreationOptions, TaskScheduler.Current)
        { }

        public ObserverToAsyncAdapter(IObserver<T> observer, TaskScheduler taskScheduler)
            : this(observer, TaskCreationOptions.None, taskScheduler)
        { }

        public ObserverToAsyncAdapter(IObserver<T> observer, TaskCreationOptions taskCreationOptions, TaskScheduler taskScheduler)
        {
            _observer = observer;
            _taskCreationOptions = taskCreationOptions;
            _taskScheduler = taskScheduler;
        }

        public void OnNext(T value)
        {
            _observer.OnNext(value);
        }

        public void OnError(Exception error)
        {
            _observer.OnError(error);
        }

        public void OnCompleted()
        {
            _observer.OnCompleted();
        }

        public Task OnNextAsync(T next)
        {
            return OnNextAsync(next, CancellationToken.None);
        }

        public Task OnErrorAsync(Exception exception)
        {
            return OnErrorAsync(exception, CancellationToken.None);
        }

        public Task OnCompletedAsync()
        {
            return OnCompletedAsync(CancellationToken.None);
        }

        public Task OnNextAsync(T next, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => _observer.OnNext(next), cancellationToken, _taskCreationOptions, _taskScheduler);
        }

        public Task OnErrorAsync(Exception exception, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => _observer.OnError(exception), cancellationToken, _taskCreationOptions, _taskScheduler);
        }

        public Task OnCompletedAsync(CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(() => _observer.OnCompleted(), cancellationToken, _taskCreationOptions, _taskScheduler);
        }
    }
}
