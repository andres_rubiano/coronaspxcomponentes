﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Simplexity.Components
{
	public class AsyncObserverExceptionHandler<T> : IAsyncObserver<T>
	{
		private readonly IAsyncObserver<T> _observer;
		private readonly IAsyncObserver<Exception> _exceptionObserver;

		public AsyncObserverExceptionHandler(IAsyncObserver<T> observer, IAsyncObserver<Exception> exceptionObserver)
		{
			_observer = observer;
			_exceptionObserver = exceptionObserver;
		}

		public void OnNext(T value)
		{
			try
			{
				_observer.OnNext(value);
			}
			catch (Exception e)
			{
				_exceptionObserver.OnNext(e);
			}
		}

		public void OnError(Exception error)
		{
			_observer.OnError(error);
		}

		public void OnCompleted()
		{
			_observer.OnCompleted();
		}

		public Task OnNextAsync(T next)
		{
			return _observer.OnNextAsync(next);
		}

		public Task OnNextAsync(T next, CancellationToken cancellationToken)
		{
			try
			{
				return _observer.OnNextAsync(next, cancellationToken);
			}
			catch (Exception e)
			{
				return _exceptionObserver.OnNextAsync(e, cancellationToken);
			}
		}

		public Task OnErrorAsync(Exception exception)
		{
			return _observer.OnErrorAsync(exception);
		}

		public Task OnErrorAsync(Exception exception, CancellationToken cancellationToken)
		{
			return _observer.OnErrorAsync(exception, cancellationToken);
		}

		public Task OnCompletedAsync()
		{
			return _observer.OnCompletedAsync();
		}

		public Task OnCompletedAsync(CancellationToken cancellationToken)
		{
			return _observer.OnCompletedAsync(cancellationToken);
		}
	}
}
