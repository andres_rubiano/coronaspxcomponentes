﻿using System;

namespace Simplexity.Components.Helpers
{
    /// <summary>
    /// Implementación de IDisposable que ejecuta una acción especificada en su creación.
    /// </summary>
    internal class ActionDisposable : IDisposable
    {
        /// <summary>
        /// Acción a ejecutar cuando se vaya a hacer disposición.
        /// </summary>
        private readonly Action _action;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="action">Acción a ejecutar cuando se vaya a hacer disposición.</param>
        public ActionDisposable(Action action)
        {
            _action = action;
        }

        /// <summary>
        /// Realiza el proceso de disposición de los recursos.
        /// En este caso particular, llama la acción especificada en el constructor.
        /// </summary>
        public void Dispose()
        {
            _action();
        }
    }
}
