﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Simplexity.Components
{
    /// <summary>
    /// Implementación base de un observador asíncrono.
    /// </summary>
    /// <typeparam name="T">Tipo del objeto que provee información de las notificaciones.</typeparam>
    public abstract class AbstractAsyncObserver<T> : IAsyncObserver<T>
    {
        public void OnNext(T value)
        {
            OnNextAsync(value, CancellationToken.None).Wait();
        }

        public void OnError(Exception error)
        {
            OnErrorAsync(error, CancellationToken.None).Wait();
        }

        public void OnCompleted()
        {
            OnCompletedAsync(CancellationToken.None).Wait();
        }

        public Task OnNextAsync(T next)
        {
            return OnNextAsync(next, CancellationToken.None);
        }

        public Task OnErrorAsync(Exception exception)
        {
            return OnErrorAsync(exception, CancellationToken.None);
        }

        public Task OnCompletedAsync()
        {
            return OnCompletedAsync(CancellationToken.None);
        }

        public abstract Task OnNextAsync(T next, CancellationToken cancellationToken);

        public abstract Task OnErrorAsync(Exception exception, CancellationToken cancellationToken);

        public abstract Task OnCompletedAsync(CancellationToken cancellationToken);
    }
}
