﻿namespace Simplexity.Components.Serialization
{
    public interface ISerializer
    {
		int Serialize<T>(T obj, byte[] bytes, int offset);
	    byte[] Serialize<T>(T obj);
		string SerializeToString<T>(T obj);

		T Deserialize<T>(byte[] bytes);
		T Deserialize<T>(byte[] bytes, int offset, int count);
		T DeserializeFromString<T>(string serialObj);
    }
}
