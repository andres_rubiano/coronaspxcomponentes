﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Simplexity.Components
{
    public class AsyncSubjectExceptionHandler<TIn, TOut> : IAsyncSubject<TIn, TOut>
	{
        private readonly IAsyncSubject<TIn, TOut> _subject;
        private readonly IAsyncObserver<Exception> _exceptionObserver;

        public AsyncSubjectExceptionHandler(IAsyncSubject<TIn, TOut> subject, IAsyncObserver<Exception> exceptionObserver)
		{
			_subject = subject;
			_exceptionObserver = exceptionObserver;
		}

        public void OnNext(TIn value)
        {
            try
			{
				_subject.OnNext(value);
			}
			catch (Exception e)
			{
				_exceptionObserver.OnNext(e);
			}
        }

        public void OnError(Exception error)
        {
            _subject.OnError(error);
        }

        public void OnCompleted()
        {
            _subject.OnCompleted();
        }

        public Task OnNextAsync(TIn next)
        {
            return _subject.OnNextAsync(next);
        }

        public Task OnNextAsync(TIn next, CancellationToken cancellationToken)
        {
            try
            {
                return _subject.OnNextAsync(next, cancellationToken);
            }
            catch (Exception e)
            {
                return _exceptionObserver.OnNextAsync(e, cancellationToken);
            }
        }

        public Task OnErrorAsync(Exception exception)
        {
            return _subject.OnErrorAsync(exception);
        }

        public Task OnErrorAsync(Exception exception, CancellationToken cancellationToken)
        {
            return _subject.OnErrorAsync(exception, cancellationToken);
        }

        public Task OnCompletedAsync()
        {
            return _subject.OnCompletedAsync();
        }

        public Task OnCompletedAsync(CancellationToken cancellationToken)
        {
            return _subject.OnCompletedAsync(cancellationToken);
        }

        public IDisposable Subscribe(IObserver<TOut> observer)
        {
            return _subject.Subscribe(observer);
        }

        public Task<IDisposable> SubscribeAsync(IAsyncObserver<TOut> observer)
        {
            return _subject.SubscribeAsync(observer);
        }

        public Task<IDisposable> SubscribeAsync(IAsyncObserver<TOut> observer, CancellationToken cancellationToken)
        {
            return _subject.SubscribeAsync(observer, cancellationToken);
        }
	}
}
