﻿using System;

namespace Simplexity.Components.Messaging
{

    /// <summary>
    /// Interfaz de un cliente de un broker de mensajería.
    /// </summary>
    /// <typeparam name="TRx">Tipo de los mensajes que se reciben desde el broker hacia el cliente.</typeparam>
    /// <typeparam name="TTx">Tipo de los mensajes que se envian desde el cliente hacia el broker.</typeparam>
    public interface IMessageBrokerClient<out TRx, in TTx> : IAsyncSubject<TTx, TRx>, IAsyncStartable, IDisposable
    {
         
    }
}