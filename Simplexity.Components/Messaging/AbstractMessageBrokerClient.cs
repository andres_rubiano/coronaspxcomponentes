﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Simplexity.Components.Messaging
{
    /// <summary>
    /// Implementación básica de un cliente de un broker de mensajería.
    /// Las clases que hereden de esta, deben implementar las propiedades que proveen el receptor y el productor de mensajes.
    /// </summary>
    /// <typeparam name="TRx">Tipo de los mensajes que se reciben desde el broker hacia el cliente.</typeparam>
    /// <typeparam name="TTx">Tipo de los mensajes que se envian desde el cliente hacia el broker.</typeparam>
    public abstract class AbstractMessageBrokerClient<TRx, TTx> : AbstractAsyncSubject<TTx, TRx>, IMessageBrokerClient<TRx, TTx>
    {
        protected abstract IMessageReceiver<TRx> MessageReceiver { get; set; }
        protected abstract IMessageSender<TTx> MessageSender { get; set; }

        public Task StartAsync()
        {
            return Task.WhenAll(
                MessageReceiver.StartAsync(),
                MessageSender.StartAsync());
        }

        public Task StopAsync()
        {
            return Task.WhenAll(
                MessageReceiver.StopAsync(),
                MessageSender.StopAsync());
        }

        public override Task OnNextAsync(TTx next, CancellationToken cancellationToken)
        {
            return MessageSender.OnNextAsync(next, cancellationToken);
        }

        public override Task OnErrorAsync(Exception exception, CancellationToken cancellationToken)
        {
            return MessageSender.OnErrorAsync(exception, cancellationToken);
        }

        public override Task OnCompletedAsync(CancellationToken cancellationToken)
        {
            return MessageSender.OnCompletedAsync(cancellationToken);
        }

        public override Task<IDisposable> SubscribeAsync(IAsyncObserver<TRx> observer, CancellationToken cancellationToken)
        {
            return MessageReceiver.SubscribeAsync(observer, cancellationToken);
        }

        public void Dispose()
        {
            if (MessageReceiver != null) MessageReceiver.Dispose();
            if (MessageSender != null) MessageSender.Dispose();
        }
    }
}
