﻿namespace Simplexity.Components.Messaging
{
	/// <summary>
	/// Interfaz para un cliente transmisor/generador de mensajes básicos(IBaseMessage).
	/// </summary>
    public interface IBaseMessageSender : IMessageSender<IBaseMessage>
    {
    }
}
