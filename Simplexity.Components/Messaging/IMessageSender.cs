﻿using System;

namespace Simplexity.Components.Messaging
{
    /// <summary>
    /// Interfaz para un cliente transmisor/generador de mensajes.
    /// </summary>
    /// <typeparam name="T">Tipo de los mensajes.</typeparam>
    public interface IMessageSender<in T> : IAsyncObserver<T>, IAsyncStartable, IDisposable
    {
         
    }
}