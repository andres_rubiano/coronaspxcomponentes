﻿namespace Simplexity.Components.Messaging
{
	/// <summary>
	/// Interfaz de un cliente receptor de mensajes básicos(IBaseMessage). Estos mensajes se exponen como un observable.
	/// </summary>
    public interface IBaseMessageReceiver : IMessageReceiver<IBaseMessage>
    {
    }
}
