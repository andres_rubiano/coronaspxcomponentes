﻿using System.Collections.Generic;

namespace Simplexity.Components.Messaging
{
	/// <summary>
	/// Representación de un mensaje básico.
	/// </summary>
    public interface IBaseMessage
    {
		/// <summary>
		/// Encabezados del mensaje.
		/// </summary>
        IEnumerable<KeyValuePair<string, object>> Headers { get; }

		/// <summary>
		/// Contenido del mensaje.
		/// </summary>
        byte[] Content { get; }
    }
}
