﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Simplexity.Components.Messaging
{
	/// <summary>
	/// Implementación del mensaje básico.
	/// </summary>
    public class BaseMessage : IBaseMessage
    {
		/// <summary>
		/// Encabezados del mensaje.
		/// </summary>
        public IEnumerable<KeyValuePair<string, object>> Headers { get; private set; }

		/// <summary>
		/// Contenido del mensaje.
		/// </summary>
        public byte[] Content { get; private set; }

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="content">Contenido del mensaje.</param>
        public BaseMessage(byte[] content)
			: this(Enumerable.Empty<KeyValuePair<string, object>>(), content)
        {
        }

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="headers">Encabezados del mensaje.</param>
		/// <param name="content">Contenido del mensaje.</param>
        public BaseMessage(IEnumerable<KeyValuePair<string, object>> headers, byte[] content)
        {
            if (headers == null)
            {
				throw new ArgumentNullException("headers");
            }
            Headers = headers;
            Content = content;
        }
    }
}
