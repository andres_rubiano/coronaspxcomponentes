﻿using System;

namespace Simplexity.Components.Messaging
{
    /// <summary>
    /// Interfaz de un cliente receptor de mensajes. Estos mensajes se exponen como un observable.
    /// </summary>
    /// <typeparam name="T">Tipo de los mensajes.</typeparam>
    public interface IMessageReceiver<out T> : IAsyncObservable<T>, IAsyncStartable, IDisposable
    {
         
    }
}