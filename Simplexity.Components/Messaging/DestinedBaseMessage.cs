﻿namespace Simplexity.Components.Messaging
{
	public class DestinedMessageContainer<T>
	{
		public string Destination { get; private set; }
		public T Message { get; private set; }

		public DestinedMessageContainer(string destination, T message)
		{
			Destination = destination;
			Message = message;
		}
	}
}
