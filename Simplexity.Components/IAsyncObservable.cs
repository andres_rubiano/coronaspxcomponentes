﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Simplexity.Components
{
    /// <summary>
    /// Interfaz observable (Patrón Observador) con asincronía.
    /// </summary>
    /// <typeparam name="T">Tipo del objeto que provee información de las notificaciones.</typeparam>
    public interface IAsyncObservable<out T> : IObservable<T>
    {
        /// <summary>
        /// Se hace una suscripción avisando al observable que debe enviar notificaciones a un nuevo observador.
        /// </summary>
        /// <param name="observer">El objeto que recibe las notificaciones de los eventos.</param>
        /// <returns>Una tarea cuyo resultado es un IDisposable que permite al llamador destruir la suscripción.</returns>
        Task<IDisposable> SubscribeAsync(IAsyncObserver<T> observer);

        /// <summary>
        /// Se hace una suscripción avisando al observable que debe enviar notificaciones a un nuevo observador.
        /// </summary>
        /// <param name="observer">El objeto que recibe las notificaciones de los eventos.</param>
        /// <param name="cancellationToken">Token para cancelación de la suscripción.</param>
        /// <returns>Una tarea asíncrona cuyo resultado es un IDisposable que permite al llamador destruir la suscripción.</returns>
        Task<IDisposable> SubscribeAsync(IAsyncObserver<T> observer, CancellationToken cancellationToken);
    }
}