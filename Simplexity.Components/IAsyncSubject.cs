﻿namespace Simplexity.Components
{
    /// <summary>
    /// Representa un objeto que es a su vez observador y observable.
    /// </summary>
    /// <typeparam name="TIn">Tipo del objeto que provee información de las notificaciones recibidas.</typeparam>
    /// <typeparam name="TOut">Tipo del objeto que provee información de las notificaciones generadas.</typeparam>
    public interface IAsyncSubject<in TIn, out TOut> : IAsyncObserver<TIn>, IAsyncObservable<TOut>
    {}


    /// <summary>
    /// Representa un objeto que es a su vez observador y observable.
    /// </summary>
    /// <typeparam name="T">Tipo del objeto que provee información de las notificaciones recibidas y generadas.</typeparam>
    public interface IAsyncSubject<T> : IAsyncSubject<T, T>
    {}
} 