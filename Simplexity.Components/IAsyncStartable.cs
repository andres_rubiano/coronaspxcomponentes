﻿using System.Threading;
using System.Threading.Tasks;

namespace Simplexity.Components
{
    /// <summary>
    /// Interfaz para representa un objeto/proceso que debería ser explícitamente iniciado.
    /// </summary>
    public interface IAsyncStartable
    {
        /// <summary>
        /// Iniciar.
        /// </summary>
        /// <returns>Tarea asíncrona.</returns>
        Task StartAsync();

        /// <summary>
        /// Iniciar.
        /// </summary>
        /// <returns>Tarea asíncrona.</returns>
        Task StopAsync();
    }
}