﻿using System;
using System.Threading.Tasks;
using System.Linq;

namespace Simplexity.Components
{
    /// <summary>
    /// Adaptador de observable síncrono a observable asíncrono.
    /// </summary>
    /// <typeparam name="T">Tipo del objeto que provee información de las notificaciones.</typeparam>
    public sealed class ObservableToAsyncAdapter<T> : AbstractAsyncObservable<T>, IObserver<T>
    {
        private readonly IObservable<T> _observable;

        public ObservableToAsyncAdapter(IObservable<T> observable)
        {
            _observable = observable;
            _observable.Subscribe(this);
        }

        public void OnNext(T value)
        {
            Task.WhenAll(Observers.Select(o => o.OnNextAsync(value)));
        }

        public void OnError(Exception error)
        {
            Task.WhenAll(Observers.Select(o => o.OnErrorAsync(error)));
        }

        public void OnCompleted()
        {
            Task.WhenAll(Observers.Select(o => o.OnCompletedAsync()));
        }
    }
}
