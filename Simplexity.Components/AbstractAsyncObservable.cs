﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Simplexity.Components.Helpers;

namespace Simplexity.Components
{
    /// <summary>
    /// Implementación base para la construcción de un IAsyncObservable.
    /// </summary>
    /// <typeparam name="T">Tipo del objeto que provee información de las notificaciones.</typeparam>
    public abstract class AbstractAsyncObservable<T> : IAsyncObservable<T>
    {
        /// <summary>
        /// Observadores de las notificaciones generadas.
        /// </summary>
        protected ObservableCollection<IAsyncObserver<T>> Observers { get; private set; }

        protected AbstractAsyncObservable()
        {
            Observers = new ObservableCollection<IAsyncObserver<T>>();
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            return SubscribeAsync(new ObserverToAsyncAdapter<T>(observer)).Result;
        }

        public Task<IDisposable> SubscribeAsync(IAsyncObserver<T> observer)
        {
            return SubscribeAsync(observer, CancellationToken.None);
        }

        public virtual Task<IDisposable> SubscribeAsync(IAsyncObserver<T> observer, CancellationToken cancellationToken)
        {
            Observers.Add(observer);
            return Task.FromResult<IDisposable>(new ActionDisposable(() => Observers.Remove(observer)));
        }

        protected Task CallObserversOnNextAsync(T next)
        {
            return CallObserversOnNextAsync(next, CancellationToken.None);
        }

        protected Task CallObserversOnExceptionAsync(Exception exception)
        {
            return CallObserversOnExceptionAsync(exception, CancellationToken.None);
        }

        protected Task CallObserversOnCompletedAsync()
        {
            return CallObserversOnCompletedAsync(CancellationToken.None);
        }

        protected Task CallObserversOnNextAsync(T next, CancellationToken cancellationToken)
        {
            return Task.WhenAll(Observers.Select(o => o.OnNextAsync(next, cancellationToken)));
        }

        protected Task CallObserversOnExceptionAsync(Exception exception, CancellationToken cancellationToken)
        {
            return Task.WhenAll(Observers.Select(o => o.OnErrorAsync(exception, cancellationToken)));
        }

        protected Task CallObserversOnCompletedAsync(CancellationToken cancellationToken)
        {
            return Task.WhenAll(Observers.Select(o => o.OnCompletedAsync(cancellationToken)));
        }
    }
}
