﻿using System;
using Simplexity.Components.Serialization;

namespace Simplexity.Components.Messaging.Nms
{
    /// <summary>
    /// Cliente para broker de mensajería usando Apache.NMS.
    /// 
    /// Para mas información sobre Apache.NMS. Ir a: http://activemq.apache.org/nms/
    /// </summary>
    /// <typeparam name="TRx">Tipo de los mensajes que se reciben desde el broker hacia el cliente.</typeparam>
    /// <typeparam name="TTx">Tipo de los mensajes que se envian desde el cliente hacia el broker.</typeparam>
    public sealed class NmsMessageBrokerClient<TRx, TTx> : AbstractMessageBrokerClient<TRx, TTx>
        where TRx : class
        where TTx : class
    {
        /// <summary>
        /// Receptor de mensajes.
        /// </summary>
        protected override IMessageReceiver<TRx> MessageReceiver { get; set; }

        /// <summary>
        /// Enviador de mensajes.
        /// </summary>
        protected override IMessageSender<TTx> MessageSender { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="brokerUri">URI al broker de mensajería. El formato de la URI debe ser el requerido por Apache.NMS.</param>
        /// <param name="sourceUri">URI a la cola de la cual se consumiran mensajes. El formato de la URI debe ser el requerido por Apache.NMS.</param>
        /// <param name="destinationUri">URI a la cola a la cual se enviaran mensajes. El formato de la URI debe ser el requerido por Apache.NMS.</param>
        /// <param name="connectionRetryInterval">Intervalo de reintento de conexiones cuando hay caidas de conexión.</param>
        /// <param name="user">Usuario para autenticarse con el broker de mensajería.</param>
        /// <param name="password">Constraseña para autenticarse con el broker de mensajería.</param>
		/// <param name="serializer">Serializador/Deserializador de objetos.</param>
        public NmsMessageBrokerClient(ISerializer serializer, Uri brokerUri, Uri sourceUri, 
			Uri destinationUri, TimeSpan connectionRetryInterval, string user = null, string password = null)
        {
            //Haciendo uso del AbstractMessageBroker, lo único que se debe hacer es construir el productor y el receptor de mensajes.
            MessageReceiver = new NmsMessageReceiver<TRx>(serializer, brokerUri, sourceUri, connectionRetryInterval, user, password);
            MessageSender = new NmsMessageSender<TTx>(serializer, brokerUri, destinationUri, connectionRetryInterval, user, password);
        }

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="config">Configuración del cliente.</param>
		/// <param name="serializer">Serializador/Deserializador de objetos.</param>
		public NmsMessageBrokerClient(NmsClientConfiguration config, ISerializer serializer)
			: this(serializer, config.BrokerUri, config.SourceUri, config.DestinationUri, config.ConnectionRetryInterval, config.User, config.Password)
		{
		}
    }
}
