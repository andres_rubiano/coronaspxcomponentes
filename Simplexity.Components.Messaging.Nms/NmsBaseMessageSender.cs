﻿using System;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Threading.Tasks;
using Apache.NMS;
using Common.Logging;
using IMessage = Apache.NMS.IMessage;

namespace Simplexity.Components.Messaging.Nms
{
	/// <summary>
	/// Cliente para enviar mensajes básicos(IBaseMessage) a broker de mensajería usando Apache.NMS.
	/// 
	/// Para mas información sobre Apache.NMS. Ir a: http://activemq.apache.org/nms/
	/// </summary>
    public class NmsBaseMessageSender : AbstractAsyncObserver<IBaseMessage>, IBaseMessageSender
    {
        /// <summary>
        /// Logger.
        /// </summary>
        private static readonly ILog _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// URI de la cola destino.
        /// </summary>
        private readonly Uri _destinationUri;

        /// <summary>
        /// Objeto para administrar las conexiones.
        /// </summary>
        private readonly NmsConnection _nmsConnection;

        /// <summary>
        /// Cliente productor de mensajes.
        /// </summary>
        private IMessageProducer _messageProducer;

        /// <summary>
        /// Lock para uso de _messageProducer. Recurso escaso.
        /// </summary>
        private readonly object _messageProducerLock;

        /// <summary>
        /// Generador de tokens para detener cualquier procesamiento.
        /// </summary>
        private readonly CancellationTokenSource _stopper;

        /// <summary>
        /// Flag to indicate if the processing has started.
        /// </summary>
        private bool _started;

        /// <summary>
        /// Señal activa cuando está conectado.
        /// </summary>
        private readonly ManualResetEventSlim _connectedLatch;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="brokerUri">URI al broker de mensajería. El formato de la URI debe ser el requerido por Apache.NMS.</param>
        /// <param name="destinationUri">URI a la cola a la cual se enviaran mensajes. El formato de la URI debe ser el requerido por Apache.NMS.</param>
        /// <param name="connectionRetryInterval">Intervalo de reintento de conexiones cuando hay caidas de conexión.</param>
        /// <param name="user">Usuario para autenticarse con el broker de mensajería.</param>
        /// <param name="password">Constraseña para autenticarse con el broker de mensajería.</param>
        public NmsBaseMessageSender(Uri brokerUri, Uri destinationUri, TimeSpan connectionRetryInterval, string user = null, string password = null)
        {
            _destinationUri = destinationUri;
            _nmsConnection = new NmsConnection(brokerUri, connectionRetryInterval, user, password);
            _nmsConnection.ConnectionCreated += OnConnectionCreated;
            _nmsConnection.ConnectionStarted += OnConnectionStarted;
            _connectedLatch = new ManualResetEventSlim(false);
            _messageProducerLock = new object();
            _stopper = new CancellationTokenSource();
            _started = false;
        }

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="config">Configuración del cliente.</param>
		public NmsBaseMessageSender(NmsClientConfiguration config)
			: this(config.BrokerUri, config.DestinationUri, config.ConnectionRetryInterval, config.User, config.Password)
		{
		}

        /// <summary>
        /// Manejador de eventos de creación de conexión.
        /// </summary>
        /// <param name="sender">Quien envia el evento.</param>
        /// <param name="tuple">Tupla con la conexión y una sesión.</param>
        private void OnConnectionCreated(object sender, Tuple<IConnection, ISession> tuple)
        {
            if (_stopper.IsCancellationRequested) return;

            _connectedLatch.Reset();

            try
            {
                ISession session = tuple.Item2;

                lock (_messageProducerLock)
                {
                    if (_messageProducer != null) _messageProducer.Dispose();
                    _messageProducer = session.CreateProducer(session.GetDestination(_destinationUri.OriginalString));
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Falló creación de productor de mensajes.", ex);
            }
        }

        /// <summary>
        /// Manejador de eventos de inicio de conexión.
        /// </summary>
        /// <param name="sender">Quien envia el evento.</param>
        /// <param name="eventArgs">Datos del evento.</param>
        private void OnConnectionStarted(object sender, EventArgs eventArgs)
        {
            _connectedLatch.Set();
        }

        /// <summary>
        /// Iniciar el procesamiento.
        /// </summary>
        /// <returns>Tarea asíncrona</returns>
        public Task StartAsync()
        {
            TaskCompletionSource<object> tcs = new TaskCompletionSource<object>();
            tcs.SetResult(null);

            lock (_stopper)
            {
                if (_started)
                    return tcs.Task;
                _started = true;
            }

            return _nmsConnection.StartAsync();
        }

        /// <summary>
        /// Detener el procesamiento.
        /// </summary>
        /// <returns>Tarea asíncrona.</returns>
        public Task StopAsync()
        {
            Task task = _nmsConnection.StopAsync();
            _stopper.Cancel();
            _connectedLatch.Set();
            return task;
        }

        /// <summary>
        /// Enviar mensaje.
        /// </summary>
        /// <param name="next">Contenido del mensaje.</param>
        /// <param name="cancellationToken">Token de cancelación.</param>
        /// <returns></returns>
        public override Task OnNextAsync(IBaseMessage next, CancellationToken cancellationToken)
        {
            if (next == null)
                throw new ArgumentNullException("next");

            return Task.Factory.StartNew(
                () =>
                {
                    while (true)
                    {
                        _connectedLatch.Wait(cancellationToken);
                        if (_stopper.IsCancellationRequested)
                            throw new OperationCanceledException(cancellationToken);

                        try
                        {
                            lock (_messageProducerLock)
                            {
                                IMessage message = _messageProducer.CreateBytesMessage(next.Content);
	                            if (next.Headers != null)
	                            {
		                            foreach (var header in next.Headers)
		                            {
			                            message.Properties[header.Key] = header.Value;
		                            }
	                            }
	                            _messageProducer.Send(message);
                                _logger.Trace(l => l("Mensaje Enviado: {0}", message.ToString()));
                                break;
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Debug("Falló Envio.", ex);
                            _connectedLatch.Reset();
                        }
                    }
                }
            , cancellationToken, TaskCreationOptions.None, TaskScheduler.Default);
        }

        public override Task OnErrorAsync(Exception exception, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override Task OnCompletedAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            StopAsync().Wait();
            if (_messageProducer != null) _messageProducer.Dispose();
        }
    }
}
