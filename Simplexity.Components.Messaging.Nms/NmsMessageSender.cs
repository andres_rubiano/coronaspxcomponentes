﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Simplexity.Components.Serialization;

namespace Simplexity.Components.Messaging.Nms
{
    /// <summary>
    /// Cliente para enviar mensajes a broker de mensajería usando Apache.NMS.
    /// 
    /// Para mas información sobre Apache.NMS. Ir a: http://activemq.apache.org/nms/
    /// </summary>
    /// <typeparam name="T">Tipo de los mensajes que se envian desde el cliente hacia el broker.</typeparam>
    public class NmsMessageSender<T> : AbstractAsyncObserver<T>, IMessageSender<T> where T : class
    {
        private readonly NmsBaseMessageSender _baseMessageSender;
		private readonly ISerializer _serializer;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="brokerUri">URI al broker de mensajería. El formato de la URI debe ser el requerido por Apache.NMS.</param>
        /// <param name="destinationUri">URI a la cola a la cual se enviaran mensajes. El formato de la URI debe ser el requerido por Apache.NMS.</param>
        /// <param name="connectionRetryInterval">Intervalo de reintento de conexiones cuando hay caidas de conexión.</param>
        /// <param name="user">Usuario para autenticarse con el broker de mensajería.</param>
        /// <param name="password">Constraseña para autenticarse con el broker de mensajería.</param>
		/// <param name="serializer">Serializador de los mensajes.</param>
        public NmsMessageSender(ISerializer serializer, Uri brokerUri, Uri destinationUri, TimeSpan connectionRetryInterval, string user = null, 
			string password = null)
        {
            _baseMessageSender = new NmsBaseMessageSender(brokerUri, destinationUri, connectionRetryInterval, user, password);
	        _serializer = serializer;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
		/// <param name="config">Configuración del cliente.</param>
		/// <param name="serializer">Serializador de los mensajes.</param>
		public NmsMessageSender(NmsClientConfiguration config, ISerializer serializer)
			: this(serializer, config.BrokerUri, config.DestinationUri, config.ConnectionRetryInterval, config.User, config.Password)
	    {
	    }

        public Task StartAsync()
        {
           return _baseMessageSender.StartAsync();
        }

        public Task StopAsync()
        {
            return _baseMessageSender.StopAsync();
        }

        public override Task OnNextAsync(T next, CancellationToken cancellationToken)
        {
	        IBaseMessage message = new BaseMessage(_serializer.Serialize(next));
            return _baseMessageSender.OnNextAsync(message, cancellationToken);
        }

        public override Task OnErrorAsync(Exception exception, CancellationToken cancellationToken)
        {
            return _baseMessageSender.OnErrorAsync(exception, cancellationToken);
        }

        public override Task OnCompletedAsync(CancellationToken cancellationToken)
        {
            return _baseMessageSender.OnCompletedAsync(cancellationToken);
        }

        public void Dispose()
        {
            _baseMessageSender.Dispose();
        }
    }
}
