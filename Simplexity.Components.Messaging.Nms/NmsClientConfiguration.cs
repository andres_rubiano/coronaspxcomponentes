﻿using System;

namespace Simplexity.Components.Messaging.Nms
{
	public class NmsClientConfiguration
	{
		public Uri BrokerUri { get; private set; }
		public Uri SourceUri { get; private set; }
		public Uri DestinationUri { get; private set; }
		public TimeSpan ConnectionRetryInterval { get; private set; }
		public string User { get; private set; }
		public string Password { get; private set; }

		public NmsClientConfiguration(Uri brokerUri, Uri sourceUri, Uri destinationUri,
			TimeSpan connectionRetryInterval, string user = null, string password = null)
		{
			BrokerUri = brokerUri;
			SourceUri = sourceUri;
			DestinationUri = destinationUri;
			ConnectionRetryInterval = connectionRetryInterval;
			User = user;
			Password = password;
		}
	}
}
