﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Simplexity.Components.Messaging.Nms
{

	/// <summary>
	/// Cliente para recibir mensajes de texto desde broker de mensajería usando Apache.NMS.
	/// 
	/// Para mas información sobre Apache.NMS. Ir a: http://activemq.apache.org/nms/
	/// </summary>
	public class NmsPlainTextMessageReceiver : AbstractAsyncObservable<string>, IMessageReceiver<string>
	{
		private readonly NmsBaseMessageReceiver _baseMessageReceiver;

		public NmsPlainTextMessageReceiver(Uri brokerUri, Uri sourceUri, TimeSpan connectionRetryInterval,
			string user = null, string password = null)
	    {
		    _baseMessageReceiver = new NmsBaseMessageReceiver(brokerUri, sourceUri, connectionRetryInterval, user, password);
	    }

		public async Task StartAsync()
		{
			await _baseMessageReceiver.SubscribeAsync(new BaseMessageForwarder(this));
			await _baseMessageReceiver.StartAsync();
		}

		public Task StopAsync()
		{
			return _baseMessageReceiver.StopAsync();
		}

		public void Dispose()
		{
			_baseMessageReceiver.Dispose();
		}

		private class BaseMessageForwarder : AbstractAsyncObserver<IBaseMessage>
		{
			/// <summary>
			/// Instancia de la clase padre.
			/// </summary>
			private readonly NmsPlainTextMessageReceiver _messageReceiver;

			public BaseMessageForwarder(NmsPlainTextMessageReceiver messageReceiver)
			{
				_messageReceiver = messageReceiver;
			}

			public override Task OnNextAsync(IBaseMessage next, CancellationToken cancellationToken)
			{
				return _messageReceiver.CallObserversOnNextAsync(Encoding.UTF8.GetString(next.Content), cancellationToken);
			}

			public override Task OnErrorAsync(Exception exception, CancellationToken cancellationToken)
			{
				return _messageReceiver.CallObserversOnExceptionAsync(exception, cancellationToken);
			}

			public override Task OnCompletedAsync(CancellationToken cancellationToken)
			{
				return _messageReceiver.CallObserversOnCompletedAsync(cancellationToken);
			}
		}
	}
}
