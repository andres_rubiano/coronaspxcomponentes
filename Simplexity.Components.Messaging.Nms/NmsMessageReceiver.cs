﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Simplexity.Components.Serialization;

namespace Simplexity.Components.Messaging.Nms
{
    /// <summary>
    /// Cliente para recibir mensajes desde broker de mensajería usando Apache.NMS.
    /// 
    /// Para mas información sobre Apache.NMS. Ir a: http://activemq.apache.org/nms/
    /// </summary>
    /// <typeparam name="T">Tipo de los mensajes que se reciben desde el broker hacia este cliente.</typeparam>
    public class NmsMessageReceiver<T> : AbstractAsyncObservable<T>, IMessageReceiver<T> where T : class
    {
		private readonly NmsBaseMessageReceiver _baseMessageReceiver;
	    private readonly ISerializer _deserializer;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="brokerUri">URI al broker de mensajería. El formato de la URI debe ser el requerido por Apache.NMS.</param>
		/// <param name="sourceUri">URI a la cola desde la cual se recibiran mensajes. El formato de la URI debe ser el requerido por Apache.NMS.</param>
		/// <param name="connectionRetryInterval">Intervalo de reintento de conexiones cuando hay caidas de conexión.</param>
		/// <param name="user">Usuario para autenticarse con el broker de mensajería.</param>
		/// <param name="password">Constraseña para autenticarse con el broker de mensajería.</param>
		/// <param name="deserializer">Deserializador de los mensajes.</param>
		public NmsMessageReceiver(ISerializer deserializer, Uri brokerUri, Uri sourceUri, TimeSpan connectionRetryInterval, string user = null,
		    string password = null)
	    {
		    _baseMessageReceiver = new NmsBaseMessageReceiver(brokerUri, sourceUri, connectionRetryInterval, user, password);
			_deserializer = deserializer;
	    }

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="config">Configuración del cliente.</param>
		/// <param name="deserializer">Deserializador de los mensajes.</param>
	    public NmsMessageReceiver(NmsClientConfiguration config, ISerializer deserializer)
			: this(deserializer, config.BrokerUri, config.SourceUri, config.ConnectionRetryInterval, config.User, config.Password)
	    {
	    }

	    public async Task StartAsync()
	    {
		    await _baseMessageReceiver.SubscribeAsync(new BaseMessageForwarder(this));
		    await _baseMessageReceiver.StartAsync();
	    }

	    public Task StopAsync()
	    {
			return _baseMessageReceiver.StopAsync();
	    }

	    public void Dispose()
	    {
		    _baseMessageReceiver.Dispose();
	    }

	    private class BaseMessageForwarder : AbstractAsyncObserver<IBaseMessage>
	    {
			/// <summary>
			/// Instancia de la clase padre.
			/// </summary>
		    private readonly NmsMessageReceiver<T> _messageReceiver;

			public BaseMessageForwarder(NmsMessageReceiver<T> messageReceiver)
			{
				_messageReceiver = messageReceiver;
		    }

		    public override Task OnNextAsync(IBaseMessage next, CancellationToken cancellationToken)
		    {
				T message = _messageReceiver._deserializer.Deserialize<T>(next.Content);
			    return _messageReceiver.CallObserversOnNextAsync(message, cancellationToken);
		    }

		    public override Task OnErrorAsync(Exception exception, CancellationToken cancellationToken)
		    {
			    return _messageReceiver.CallObserversOnExceptionAsync(exception, cancellationToken);
		    }

		    public override Task OnCompletedAsync(CancellationToken cancellationToken)
		    {
				return _messageReceiver.CallObserversOnCompletedAsync(cancellationToken);
		    }
	    }
    }
}
