﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Apache.NMS;
using Common.Logging;

namespace Simplexity.Components.Messaging.Nms
{
    public class NmsConnection : IAsyncStartable
    {
        /// <summary>
        /// Logger.
        /// </summary>
        private static readonly ILog _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Intervalo de reintento de conexiones cuando hay caidas de conexión.
        /// </summary>
        private readonly TimeSpan _connectionRetryInterval;

        /// <summary>
        /// Usuario.
        /// </summary>
        private readonly string _user;

        /// <summary>
        /// Constraseña.
        /// </summary>
        private readonly string _password;

        /// <summary>
        /// Conexión Actual.
        /// </summary>
        private IConnection _connection;

        /// <summary>
        /// Sesión Actual.
        /// </summary>
        private ISession _session;


        /// <summary>
        /// Fábrica de conéxiones NMS.
        /// </summary>
        private readonly NMSConnectionFactory _connectionFactory;

        /// <summary>
        /// Señal para detener procesamiento.
        /// </summary>
        private readonly CancellationTokenSource _stopper;

        /// <summary>
        /// Flag to indicate if the processing has started.
        /// </summary>
        private bool _started;

        /// <summary>
        /// Señal de desconexión.
        /// </summary>
        private readonly ManualResetEventSlim _disconnectedLatch;
        
        /// <summary>
        /// Evento de creación de conexión.
        /// </summary>
        public event EventHandler<Tuple<IConnection, ISession>> ConnectionCreated;

        /// <summary>
        /// Evento de iniciación de conexión.
        /// </summary>
        public event EventHandler ConnectionStarted;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="brokerUri">URI al broker de mensajería. El formato de la URI debe ser el requerido por Apache.NMS.</param>
        /// <param name="connectionRetryInterval">Intervalo de reintento de conexiones cuando hay caidas de conexión.</param>
        /// <param name="user">Usuario para autenticarse con el broker de mensajería.</param>
        /// <param name="password">Constraseña para autenticarse con el broker de mensajería.</param>
        public NmsConnection(Uri brokerUri, TimeSpan connectionRetryInterval, string user = null, string password = null)
        {
            _connectionRetryInterval = connectionRetryInterval;
            _user = user;
            _password = password;
            _connectionFactory = new NMSConnectionFactory(brokerUri.OriginalString);
            _disconnectedLatch = new ManualResetEventSlim(false);
            _stopper = new CancellationTokenSource();
            _started = false;
        }

        /// <summary>
        /// Iniciar el procesamiento.
        /// </summary>
        /// <returns>Tarea asíncrona</returns>
        public Task StartAsync()
        {
            TaskCompletionSource<object> tcs = new TaskCompletionSource<object>();
            tcs.SetResult(null);

            lock (_stopper)
            {
                if (_started)
                    return tcs.Task;
                _started = true;
            }

            _disconnectedLatch.Set();

            Task.Factory.StartNew(
                async () =>
                    {
                        while(!_stopper.IsCancellationRequested)
                        {
                            _disconnectedLatch.Wait(_stopper.Token);                            
                            _disconnectedLatch.Reset();

                            try
                            {
                                _connection = _connectionFactory.CreateConnection(_user, _password);
                                _connection.ExceptionListener += OnConnectionException;
                                _session = _connection.CreateSession(AcknowledgementMode.IndividualAcknowledge);
                                OnConnectionCreated(new Tuple<IConnection, ISession>(_connection, _session));
                                _connection.Start();
                                OnConnectionStarted(null);
                            }
                            catch (Exception ex)
                            {
                                _logger.Debug(l => l("Intento de conexión fallido a {0}. Se reintentará en: {1}.", _connectionFactory.BrokerUri, _connectionRetryInterval), ex);
                                _disconnectedLatch.Set();
                            }
                            await Task.Delay(_connectionRetryInterval);
                        }
                    }, _stopper.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);

            return tcs.Task;
        }

        /// <summary>
        /// Detener el procesamiento.
        /// </summary>
        /// <returns>Tarea asíncrona.</returns>
        public Task StopAsync()
        {
            _stopper.Cancel();

            var tcs = new TaskCompletionSource<object>();
            tcs.SetResult(null);
            return tcs.Task;
        }


        /// <summary>
        /// Invocador de los manejadores de evento de conexión.
        /// </summary>
        /// <param name="e">Datos del evento.</param>
        protected virtual void OnConnectionCreated(Tuple<IConnection, ISession> e)
        {
            _logger.Debug("Conexión con el broker creada.");
            EventHandler<Tuple<IConnection, ISession>> handler = ConnectionCreated;
            if (handler != null) handler(this, e);
        }

        /// <summary>
        /// Invocador de los manejadores de inicio de conexión.
        /// </summary>
        /// <param name="e">Datos del evento.</param>
        public void OnConnectionStarted(EventArgs e)
        {
            EventHandler handler = ConnectionStarted;
            if (handler != null) handler(this, e);
        }

        /// <summary>
        /// 'Handler' de error en la conexión.
        /// </summary>
        /// <param name="exception"></param>
        private void OnConnectionException(Exception exception)
        {
            _logger.Warn("Desconexión del broker.", exception);
            _disconnectedLatch.Set();
        }
    }
}
