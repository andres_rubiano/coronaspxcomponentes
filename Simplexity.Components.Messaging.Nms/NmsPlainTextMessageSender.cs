﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Simplexity.Components.Messaging.Nms
{
    /// <summary>
    /// Cliente para enviar mensajes de texto a broker de mensajería usando Apache.NMS.
    /// 
    /// Para mas información sobre Apache.NMS. Ir a: http://activemq.apache.org/nms/
    /// </summary>
    public class NmsPlainTextMessageSender : AbstractAsyncObserver<string>, IMessageSender<string>
    {
        private readonly NmsBaseMessageSender _baseMessageSender;

                /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="brokerUri">URI al broker de mensajería. El formato de la URI debe ser el requerido por Apache.NMS.</param>
        /// <param name="destinationUri">URI a la cola a la cual se enviaran mensajes. El formato de la URI debe ser el requerido por Apache.NMS.</param>
        /// <param name="connectionRetryInterval">Intervalo de reintento de conexiones cuando hay caidas de conexión.</param>
        /// <param name="user">Usuario para autenticarse con el broker de mensajería.</param>
        /// <param name="password">Constraseña para autenticarse con el broker de mensajería.</param>
        public NmsPlainTextMessageSender(Uri brokerUri, Uri destinationUri, TimeSpan connectionRetryInterval, string user = null, string password = null)
        {
            _baseMessageSender = new NmsBaseMessageSender(brokerUri, destinationUri, connectionRetryInterval, user, password);
        }

		public Task StartAsync()
		{
			return _baseMessageSender.StartAsync();
		}

		public Task StopAsync()
		{
			return _baseMessageSender.StopAsync();
		}

        public override Task OnNextAsync(string next, CancellationToken cancellationToken)
        {
            return _baseMessageSender.OnNextAsync(new BaseMessage(Encoding.UTF8.GetBytes(next)), cancellationToken);
        }

        public override Task OnErrorAsync(Exception exception, CancellationToken cancellationToken)
        {
			return _baseMessageSender.OnErrorAsync(exception, cancellationToken);
        }

        public override Task OnCompletedAsync(CancellationToken cancellationToken)
        {
	        return _baseMessageSender.OnCompletedAsync(cancellationToken);
        }

        public void Dispose()
        {
            _baseMessageSender.Dispose();
        }
    }
}
