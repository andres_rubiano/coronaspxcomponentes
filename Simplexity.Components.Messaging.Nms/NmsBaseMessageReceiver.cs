﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Apache.NMS;
using Common.Logging;

namespace Simplexity.Components.Messaging.Nms
{
	/// <summary>
	/// Cliente para recibir mensajes básicos(IBaseMessage) desde broker de mensajería usando Apache.NMS.
	/// 
	/// Para mas información sobre Apache.NMS. Ir a: http://activemq.apache.org/nms/
	/// </summary>
	/// <typeparam name="T">Tipo de los mensajes que se reciben desde el broker hacia este cliente.</typeparam>
	public class NmsBaseMessageReceiver : AbstractAsyncObservable<IBaseMessage>, IBaseMessageReceiver
	{
        /// <summary>
        /// Logger.
        /// </summary>
        private static readonly ILog _logger = LogManager.GetCurrentClassLogger();
        
        /// <summary>
        /// Uri de la cola de origen.
        /// </summary>
        private readonly Uri _sourceUri;

        /// <summary>
        /// Objeto para administrar las conexiones.
        /// </summary>
        private readonly NmsConnection _nmsConnection;

        /// <summary>
        /// Cliente consumidor de mensajes.
        /// </summary>
        private IMessageConsumer _messageConsumer;

        /// <summary>
        /// Generador de tokens para detener cualquier procesamiento.
        /// </summary>
        private readonly CancellationTokenSource _stopper;

        /// <summary>
        /// Flag to indicate if the processing has started.
        /// </summary>
        private bool _started;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="brokerUri">URI al broker de mensajería. El formato de la URI debe ser el requerido por Apache.NMS.</param>
        /// <param name="sourceUri">URI a la cola de la cual se consumiran mensajes. El formato de la URI debe ser el requerido por Apache.NMS.</param>
        /// <param name="connectionRetryInterval">Intervalo de reintento de conexiones cuando hay caidas de conexión.</param>
        /// <param name="user">Usuario para autenticarse con el broker de mensajería.</param>
        /// <param name="password">Constraseña para autenticarse con el broker de mensajería.</param>
		public NmsBaseMessageReceiver(Uri brokerUri, Uri sourceUri, TimeSpan connectionRetryInterval, string user = null, string password = null)
        {
            _sourceUri = sourceUri;
            _nmsConnection = new NmsConnection(brokerUri, connectionRetryInterval, user, password);
            _nmsConnection.ConnectionCreated += OnConnectionCreated;
            _stopper = new CancellationTokenSource();
            _started = false;
        }

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="config">Configuración del cliente.</param>
		public NmsBaseMessageReceiver(NmsClientConfiguration config)
			: this(config.BrokerUri, config.SourceUri, config.ConnectionRetryInterval, config.User, config.Password)
		{
		}

        /// <summary>
        /// Manejador de eventos de conexión.
        /// </summary>
        /// <param name="sender">Quien envia el evento.</param>
        /// <param name="tuple">Tupla con la conexión y una sesión.</param>
        private void OnConnectionCreated(object sender, Tuple<IConnection, ISession> tuple)
        {
            if (_stopper.IsCancellationRequested) return;

            try
            {
                ISession session = tuple.Item2;
                if (_messageConsumer != null) _messageConsumer.Dispose();
                _messageConsumer = session.CreateConsumer(session.GetDestination(_sourceUri.OriginalString));
                _messageConsumer.Listener += OnMessageConsumed;
            }
            catch (Exception ex)
            {
                _logger.Error("Falló creación de productor de mensajes.", ex);
            }
        }

        /// <summary>
        /// Manejador de evento de mensaje recibido.
        /// </summary>
        /// <param name="message">Mensaje recibido.</param>
        private void OnMessageConsumed(IMessage message)
        {
            if (_stopper.IsCancellationRequested) return;

			IBytesMessage bytesMessage = message as IBytesMessage;
	        IDictionary<string, object> headers = new Dictionary<string, object>();
			byte[] content;

			if (bytesMessage == null)
            {
				ITextMessage textMessage = message as ITextMessage;
	            if (textMessage == null)
	            {
		            _logger.Warn(l => l("Mensaje no se pudo interpretar: {0}", message.ToString()));
					return;
	            }
	            content = Encoding.UTF8.GetBytes(textMessage.Text);
            }
            else
			{
				content = bytesMessage.Content;
			}

	        foreach (string headerName in message.Properties.Keys.Cast<string>())
	        {
		        headers.Add(headerName, message.Properties[headerName]);
	        }

            try
            {
				IBaseMessage baseMessage = new BaseMessage(headers, content);
                foreach (IAsyncObserver<IBaseMessage> observer in Observers)
                {
                    observer.OnNext(baseMessage);
                }

                message.Acknowledge();
            }
            catch (Exception ex)
            {
                //Si cualquiera de los observadores falla. No se hace Acknowledge del mensaje.
                _logger.Warn(l => l("Mensaje no procesado: {0}", message.ToString()), ex);
            }
        }


        /// <summary>
        /// Iniciar el procesamiento.
        /// </summary>
        /// <returns>Tarea asíncrona</returns>
        public Task StartAsync()
        {
            TaskCompletionSource<object> tcs = new TaskCompletionSource<object>();
            tcs.SetResult(null);

            lock (_stopper)
            {
                if (_started)
                    return tcs.Task;
                _started = true;
            }

            return _nmsConnection.StartAsync();
        }

        /// <summary>
        /// Detener el procesamiento.
        /// </summary>
        /// <returns>Tarea asíncrona.</returns>
        public Task StopAsync()
        {
            Task task = _nmsConnection.StopAsync();
            _stopper.Cancel();
            return task;
        }

        /// <summary>
        /// Disponer este objeto.
        /// 
        /// Una vez este objeto ha sido 'dispuesto'. El objeto no se debe reutilizar.
        /// </summary>
        public void Dispose()
        {
            StopAsync().Wait();
            if (_messageConsumer != null) _messageConsumer.Dispose();
        }
	}
}
