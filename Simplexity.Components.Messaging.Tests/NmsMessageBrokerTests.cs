﻿using System;
using System.Threading;
using Simplexity.Components.Messaging.Nms;
using Simplexity.Components.Serialization.JSON;
using Xunit;

namespace Simplexity.Components.Messaging.Tests
{
    public class NmsMessageBrokerTests
    {
        [Fact]
        public void BasicTest()
        {
            IMessageBrokerClient<string, string> messageBroker =
                new NmsMessageBrokerClient<string, string>(
					new JSONSerializer(), 
                    new Uri("stomp:tcp://localhost:61613"),
                    new Uri("queue://test"),
                    new Uri("queue://test"),
                    TimeSpan.FromMilliseconds(3000),
                    "admin",
                    "password");

            string messageSent = "Hola Mundo!";
            string messageReceived = string.Empty;
            int receivedCount = 0;

            messageBroker.Subscribe((message) => { messageReceived = message; receivedCount++; });
            messageBroker.StartAsync();

            messageBroker.OnNext(messageSent);
            
            Thread.Sleep(1000);

            Assert.Equal(1, receivedCount);
            Assert.Equal(messageSent, messageReceived);
        }
    }
}
