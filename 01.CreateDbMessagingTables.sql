CREATE SCHEMA Messaging;
GO

CREATE TABLE Messaging.Messages(
	id INT IDENTITY(1,1)
		CONSTRAINT pk_Messages_id PRIMARY KEY,
	destinationName NVARCHAR(128),
	headers VARBINARY(256),
	content VARBINARY(2048)
);