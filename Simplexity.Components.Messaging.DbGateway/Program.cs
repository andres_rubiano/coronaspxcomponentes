﻿using System.Configuration;
using System.Security.AccessControl;
using Simplexity.Components.Messaging.DbGateway.Properties;
using Simplexity.Components.Serialization.JSON;
using Topshelf;

namespace Simplexity.Components.Messaging.DbGateway
{
    class Program
    {
        private const string NameServiceQa = "Simplexity.Messaging.DbGateway_QA";
        private const string NameServiceDev = "Simplexity.Messaging.DbGateway_DEV";
        private const string NameServiceProd = "Simplexity.Messaging.DbGateway";

        static void Main(string[] args)
        {
            SqlServerDbExtractor dbExtractor = new SqlServerDbExtractor(
            ConfigurationManager.ConnectionStrings["Portal"].ConnectionString,
            Settings.Default.dbQueryInterval,
            Settings.Default.dbQueryReadCount,
            new JSONSerializer());


            NmsBaseMessageSenderPool messageSender = new NmsBaseMessageSenderPool(
            Settings.Default.MessageBrokerUri,
            Settings.Default.MessageBrokerConnectionRetryInterval,
            Settings.Default.MessageBrokerUser,
            Settings.Default.MessageBrokerPassword);

            dbExtractor.Subscribe(messageSender);
            string nameService;

            // Iniciar Servicio.
            HostFactory.Run(hostConfig =>
            {
                hostConfig.Service<object>(
                serviceConfig =>
                {
                    serviceConfig.ConstructUsing(() => new object());
                    serviceConfig.WhenStarted(async _ => await dbExtractor.StartAsync());
                    serviceConfig.WhenStopped(async _ => await dbExtractor.StopAsync());
                });

                hostConfig.RunAsLocalSystem();
                hostConfig.SetDescription("Simplexity.Messaging.DbGateway_DEV");
                hostConfig.SetDisplayName("Simplexity.Messaging.DbGateway_DEV");
                hostConfig.SetServiceName("Simplexity.Messaging.DbGateway_DEV");
            });
        }
    }
}
