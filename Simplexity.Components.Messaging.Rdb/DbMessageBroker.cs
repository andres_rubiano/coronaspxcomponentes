﻿namespace Simplexity.Components.Messaging.Rdb
{
    public class DbMessageBroker<TRx, TTx> : AbstractMessageBrokerClient<TRx, TTx>
        where TRx : class
        where TTx : class
    {
        protected override IMessageReceiver<TRx> MessageReceiver { get; set; }
        protected override IMessageSender<TTx> MessageSender { get; set; }
    }
}
