﻿using System;
using System.Threading.Tasks;

namespace Simplexity.Components.Messaging.Rdb
{
    public class DbMessageReceiver<T> : AbstractAsyncObservable<T>, IMessageReceiver<T> where T : class
    {
        public Task StartAsync()
        {
            throw new NotImplementedException();
        }

        public Task StopAsync()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
