﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Simplexity.Components.Messaging.Rdb
{
    public class DbMessageSender<T> : AbstractAsyncObserver<T>, IMessageSender<T> where T : class
    {
        public override Task OnNextAsync(T next, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override Task OnErrorAsync(Exception exception, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public override Task OnCompletedAsync(CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task StartAsync()
        {
            throw new NotImplementedException();
        }

        public Task StopAsync()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
