﻿using System.Configuration;
using Simplexity.Components.Messaging.RedTomo.Properties;
using Simplexity.Components.Serialization.JSON;
using Topshelf;

namespace Simplexity.Components.Messaging.RedTomo
{
    class Program
    {
        static void Main(string[] args)
        {
	        SqlServerDbExtractor dbExtractor = new SqlServerDbExtractor(
				ConfigurationManager.ConnectionStrings["Portal"].ConnectionString,
				Settings.Default.dbQueryInterval,
				Settings.Default.dbQueryReadCount,
				new JSONSerializer());


	        NmsBaseMessageSenderPool messageSender = new NmsBaseMessageSenderPool(
		        Settings.Default.MessageBrokerUri,
		        Settings.Default.MessageBrokerConnectionRetryInterval,
		        Settings.Default.MessageBrokerUser,
		        Settings.Default.MessageBrokerPassword);

	        dbExtractor.Subscribe(messageSender);

			// Iniciar Servicio.
			HostFactory.Run(hostConfig =>
			{
				hostConfig.Service<object>(
					serviceConfig =>
					{
						serviceConfig.ConstructUsing(() => new object());
						serviceConfig.WhenStarted(async _ => await dbExtractor.StartAsync());
						serviceConfig.WhenStopped(async _ => await dbExtractor.StopAsync());
					});

				hostConfig.RunAsLocalSystem();

				hostConfig.SetDescription("Simplexity.Messaging.DbGateway");
				hostConfig.SetDisplayName("Simplexity.Messaging.DbGateway");
				hostConfig.SetServiceName("Simplexity.Messaging.DbGateway");
			});
        }
    }
}
