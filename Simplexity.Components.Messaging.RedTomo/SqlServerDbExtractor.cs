﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using Common.Logging;
using Simplexity.Components.Serialization;

namespace Simplexity.Components.Messaging.RedTomo
{
    public class SqlServerDbExtractor : AbstractAsyncObservable<DestinedMessageContainer<IBaseMessage>>
    {
	    private static readonly ILog _logger = LogManager.GetCurrentClassLogger();
		private static readonly TransactionOptions _txOpts =
			new TransactionOptions(){ IsolationLevel = IsolationLevel.RepeatableRead };

	    private const int _headersMaxLen = 256;
		private const int _contentMaxLen = 2048;
        private const int _bufMaxLen = _headersMaxLen < _contentMaxLen ? _contentMaxLen : _headersMaxLen;

//		private const string _getMessagesSql =
//			@"SELECT TOP (@count) id, destinationName, headers, content
//                FROM Messaging.Messages";

//		private const string _removeMessageSql =
//			@"DELETE FROM Messaging.Messages WHERE id = @id";

		private const string _popMessagesSql =
			@"DELETE TOP (@count)
				FROM Messaging.Messages
				OUTPUT deleted.destinationName, deleted.headers, deleted.content";

        private readonly string _dbConnString;
	    private readonly TimeSpan _dbQueryInterval;
		private readonly int _dbQueryReadCount;
	    private readonly ISerializer _headersDeserializer;
	    private readonly CancellationTokenSource _cancellationTokenSource;

        public SqlServerDbExtractor(string dbConnString, TimeSpan dbQueryInterval,
			int dbQueryReadCount, ISerializer headersDeserializer)
        {
            _dbConnString = dbConnString;
	        _dbQueryInterval = dbQueryInterval;
	        _dbQueryReadCount = dbQueryReadCount;
	        _headersDeserializer = headersDeserializer;

			_cancellationTokenSource = new CancellationTokenSource();
        }

	    public async Task StartAsync()
	    {
		    ChannelMessagesFromDbToBrokerAsync();
	    }

        async public Task ChannelMessagesFromDbToBrokerAsync()
        {
	        CancellationToken cancelToken = _cancellationTokenSource.Token;
			
			SqlCommand popMessagesSqlCmd = new SqlCommand(_popMessagesSql);
			popMessagesSqlCmd.Parameters.AddWithValue("count", _dbQueryReadCount);

			byte[] buf = new byte[_bufMaxLen];

	        while (!cancelToken.IsCancellationRequested)
		    {
                try
                {
					using (TransactionScope txScope = 
						new TransactionScope(TransactionScopeOption.Required, _txOpts, 
							TransactionScopeAsyncFlowOption.Enabled))
	                using (SqlConnection conn = new SqlConnection(_dbConnString))
	                {
						popMessagesSqlCmd.Connection = conn;
						await conn.OpenAsync(cancelToken);

						using (SqlDataReader reader = 
								await popMessagesSqlCmd.ExecuteReaderAsync(cancelToken))
		                {
							if (!reader.HasRows)
							{
								await Task.Delay(_dbQueryInterval, cancelToken);
								continue;
							}

							while (await reader.ReadAsync(cancelToken))
							{
								long bufLen;

								// Extraer datos de la consulta
								string destinationName = reader.GetString(0);

								bufLen = reader.GetBytes(1, 0, buf, 0, buf.Length);

								IEnumerable<KeyValuePair<string, object>> headers =
									_headersDeserializer.Deserialize<IDictionary<string, object>>(buf, 0, (int)bufLen);

								bufLen = reader.GetBytes(2, 0, buf, 0, buf.Length);
								byte[] content = new byte[bufLen];
								Buffer.BlockCopy(buf, 0, content, 0, (int)bufLen);
								
								// Llamar a observadores.
								BaseMessage message = new BaseMessage(headers, content);
								DestinedMessageContainer<IBaseMessage> destinedMessage =
									new DestinedMessageContainer<IBaseMessage>(destinationName, message);
								await CallObserversOnNextAsync(destinedMessage, cancelToken);
							}
							reader.Close();
		                }
						conn.Close();
						txScope.Complete();
	                }
		        }
                catch (Exception ex)
	            {
				    _logger.Error(l => l("Exception while channeling messages from db to message broker."), ex);
	            }
	        }
	        
	        
        }

	    public Task StopAsync()
	    {
			TaskCompletionSource<object> tcs = new TaskCompletionSource<object>();
		    _cancellationTokenSource.Cancel();
			tcs.SetResult(null);
		    return tcs.Task;
	    }
    }
}
