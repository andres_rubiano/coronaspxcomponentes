﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Simplexity.Components.Messaging.Nms;

namespace Simplexity.Components.Messaging.RedTomo
{
	public class NmsBaseMessageSenderPool : AbstractAsyncObserver<DestinedMessageContainer<IBaseMessage>>
	{
		private readonly IDictionary<string, IBaseMessageSender> _sendersCache;
		private readonly Uri _brokerUri;
		private readonly TimeSpan _connectionRetryInterval;
		private readonly string _user;
		private readonly string _password;


		public NmsBaseMessageSenderPool(Uri brokerUri, TimeSpan connectionRetryInterval, 
			string user = null, string password = null)
		{
			_brokerUri = brokerUri;
			_connectionRetryInterval = connectionRetryInterval;
			_user = user;
			_password = password;

			_sendersCache = new Dictionary<string, IBaseMessageSender>();
		}

		public override async Task OnNextAsync(DestinedMessageContainer<IBaseMessage> next,
			CancellationToken cancellationToken)
		{
			// Obtener el IMessageSender
			IBaseMessageSender messageSender;
			string destination = ConvertDestinationNameToNmsFormat(next.Destination);
			if (!_sendersCache.TryGetValue(destination, out messageSender))
			{
				messageSender = new NmsBaseMessageSender(_brokerUri, new Uri(destination),
					_connectionRetryInterval, _user, _password);
				await messageSender.StartAsync();
				_sendersCache.Add(destination, messageSender);
			}

			// Enviar mensaje.
			await messageSender.OnNextAsync(next.Message, cancellationToken);
		}

		private static string ConvertDestinationNameToNmsFormat(string destinationName)
		{
			string[] substrs = destinationName.Split(new[] { '/' }, 2, StringSplitOptions.RemoveEmptyEntries);
			return substrs[0] + "://" + substrs[1];
		}

		public override Task OnErrorAsync(Exception exception, CancellationToken cancellationToken)
		{
			throw new NotImplementedException();
		}

		public override Task OnCompletedAsync(CancellationToken cancellationToken)
		{
			throw new NotImplementedException();
		}
	}
}
